FROM docker:stable
LABEL maintainer="dataportal-edirex@ics.muni.cz"

RUN apk --update add --no-cache py-pip build-base python-dev && \
    apk --update add --no-cache curl libffi-dev openssl-dev && \
    apk --update add  bash openssh-client && \
    pip install docker-compose
